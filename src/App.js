import Chat from './components/Chat/Chat';
import './App.css';

const dataURL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

function App() {
  return (
    <div className="App">
      <Chat url={dataURL}/>
    </div>
  );
}

export default App;
