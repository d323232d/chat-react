import React from 'react';
import './Content.css';
import Message from './Message/Message';
import UserMessage from './UserMessage/UserMessage';
import moment from "moment";
import Textarea from "./Textarea/Textarea";

const Content = (props) => {

  let messages = props.data;
  let numberMessages = messages.length;
  let participants = new Set(messages?.map(item => item.userId)) || 0;
  let lastMessage = messages.length ? messages[messages.length - 1] : null;

  return (
    <div className="content">
      <header className="header chat__info container">
        <ul className="left ">
          <li className="header-title">Chat name: Friends</li>
          <li className="header-users-count">{participants ? participants.size : 0}</li>
          <li className="header-messages-count">{numberMessages}</li>
        </ul>
        <div className="header-last-message-date right">{lastMessage ? moment(lastMessage.createdAt).format("DD.MM.yyyy HH:mm") : ' '}</div>
      </header>
      <div className="message-list container">
        <ul className="collection">
          <div className="messages-divider"><span>Today</span></div>
          {
            messages.map(item => {
              return item.user === "Julia"
                ? <UserMessage message={item} key={item.id} deleteMessage={props.deleteMessage} setIsEdit={props.setIsEdit} edit={props.edit} />
                : <Message message={item} key={item.id} />
            })
          }
        </ul>
      </div>
      <Textarea addMessage={props.addMessage} isEdit={props.isEdit} editMessageId={props.editMessageId} setEditMessage={props.edit} />
    </div>
  )
};

export default Content;