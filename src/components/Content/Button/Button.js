import React from 'react';

const Button = ({textBtn, onClick}) => {

  // const addMessage = () => {

  // }
  return (
    <button 
      className="message-input-button btn waves-effect waves-light red lighten-2" 
      type="submit" 
      name="action"
      onClick={onClick}
    >{textBtn} <i className="material-icons right">send</i>
    </button>
  )
}

export default Button;