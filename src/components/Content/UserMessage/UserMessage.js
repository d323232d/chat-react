import React from 'react';
import moment from "moment";

const UserMessage = (props) => {
  let message = props.message;

  const onHendleDelete = (e) => {
    e.preventDefault();
    const target = e.target;
    if (target.closest('.message')) {
      const messageId = target.closest('.message').id;
      props.deleteMessage(messageId);
    }
  };

  const onHendleEdit = (e, id) => {
    e.preventDefault();
    const target = e.target;
    if (target.closest('.message')) {
      props.setIsEdit(id);
      const textInput = document.querySelector('.message-input-text');
      textInput.value = message.text;
    }
  }

  return (
    <li id={message.id} className="message right collection-item own-message" data-userid={message.userId}>
      <div className="message-intro">
        {/* <div className="message-user-name">{message.user}</div> */}
        <div className="message-text">{message.text}</div>
        <div className="message-time">{moment(message.createdAt).format("HH:mm")}</div>
      </div>
      <a href="/" className="message-edit"><i className="material-icons" onClick={e => onHendleEdit(e, message.id)}>edit</i></a>
      <a href="/" className="message-delete"><i className="material-icons" onClick={e => onHendleDelete(e)}>clear</i></a>
    </li>
  )
}

export default UserMessage;