import React from 'react';
import Button from '../Button/Button';

const Textarea = ({ addMessage, isEdit, setEditMessage }) => {

  const textRef = React.createRef();
  let newMessage;
  const getMessageText = () => {
    newMessage = textRef.current.value;

  };

  const createMessage = () => {
    let mockData = {
      "avatar": "https://img.icons8.com/cute-clipart/452/hello-kitty.png",
      "createdAt": new Date().toISOString(),
      "editedAt": '',
      "id": Date.now() + '',
      "text": newMessage,
      "user": "Julia",
      "userId": Date.now() + 'user'
    };

    if (newMessage.trim().length) {
      addMessage(mockData);
      textRef.current.value = '';
    }
  };

  const editMessage = () => {
    let editedMessageText = textRef.current.value;
    if (editedMessageText.trim().length) {
      setEditMessage(editedMessageText);
      textRef.current.value = '';
    }
  }

  return (
    <div className="message-input container input-field">
      <textarea ref={textRef} className="message-input-text materialize-textarea" id="textarea1" onChange={getMessageText}></textarea>
      <Button textBtn={isEdit ? "Edit" : "Send"} onClick={isEdit ? editMessage : createMessage} />
    </div>
  )
};

export default Textarea;