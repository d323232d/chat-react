import React from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import { Loading } from '../Loader/Loader';
import Content from '../Content/Content';
import './Chat.css';

const fetchData = async (url) => {
  const data = await fetch(url)
    .then(response => response.json())
  return data;
};

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {      
      messages: [],
      isLoaded: false,
      isEdit: false,
      editMessageId: null
    };
  }

  componentDidMount() {
    fetchData(this.props.url)
      .then(data => {
        this.setState({
          messages: [...data],
          // messages: [],
          isLoaded: true,

        })
      });
  }

  addMessage = (messageObject) => {
    this.setState({
      messages: [...this.state.messages, messageObject]
    })
  }

  deleteMessage = (id) => {
    const newList = this.state.messages.filter(item => item.id !== id);
    this.setState({
      messages: [...newList]
    })
  }

  setIsEdit = (id) => {
    this.setState({
      isEdit: true,
      editMessageId: id
    })
  }

  edit = (text) => {
    const messageIndex = this.state.messages.findIndex((item) => item.id === this.state.editMessageId);
    let messageForEdit = this.state.messages.find((item) => item.id === this.state.editMessageId);
    messageForEdit = {
      ...messageForEdit,
      text: text,
      editedAt: new Date().toISOString()
    };

    let updatedMessagesList = this.state.messages;
    updatedMessagesList[messageIndex] = messageForEdit;
    this.setState({
      messages: [...updatedMessagesList],
      isEdit: false,
      editMessageId: null
    })
  }

  render() {
    return (
      <>
        <Header />
        <main className="chat">
          {this.state.isLoaded
            ? <Content
              data={this.state.messages}
              addMessage={this.addMessage}
              deleteMessage={this.deleteMessage}
              isEdit={this.state.isEdit}
              setIsEdit={this.setIsEdit}
              edit={this.edit}
              editMessageId={this.editMessageId}
            />
            : <Loading />}
        </main>
        <Footer />
      </>
    )
  }
}

export default Chat;